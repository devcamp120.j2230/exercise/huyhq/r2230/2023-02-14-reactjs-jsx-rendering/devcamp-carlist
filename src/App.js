import { gJsonCarsObj, checkDate } from "./info";

function App() {
  return (
    <div className="App">
      <ul>
        {gJsonCarsObj.map((value, i) => {
          return <div key={i}>
            <li>{value.make + " " + value.vID}  {checkDate(value.year)}</li>
          </div>;
        })}
      </ul>
    </div>
  );
}

export default App;
